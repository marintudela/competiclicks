﻿namespace ProyectoDani
{
    partial class UserControl1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTextoDelUsuario = new System.Windows.Forms.TextBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.btnCambioColor = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtTextoDelUsuario
            // 
            this.txtTextoDelUsuario.Location = new System.Drawing.Point(450, 191);
            this.txtTextoDelUsuario.Name = "txtTextoDelUsuario";
            this.txtTextoDelUsuario.Size = new System.Drawing.Size(100, 20);
            this.txtTextoDelUsuario.TabIndex = 0;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(238, 78);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(87, 13);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "ESTAMOS BIEN";
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(354, 272);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 2;
            this.btnEjecutar.Text = "EJECUTAR";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // btnCambioColor
            // 
            this.btnCambioColor.Location = new System.Drawing.Point(363, 214);
            this.btnCambioColor.Name = "btnCambioColor";
            this.btnCambioColor.Size = new System.Drawing.Size(94, 23);
            this.btnCambioColor.TabIndex = 3;
            this.btnCambioColor.Text = "EJECUTAR 2";
            this.btnCambioColor.UseVisualStyleBackColor = true;
            this.btnCambioColor.Click += new System.EventHandler(this.btnCambioColor_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "tabla";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCambioColor);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtTextoDelUsuario);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(800, 450);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTextoDelUsuario;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Button btnCambioColor;
        private System.Windows.Forms.Button button1;
    }
}
