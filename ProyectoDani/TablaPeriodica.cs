﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDani
{
    public partial class TablaPeriodica : Form
    {
        static string[] TablaElementos = new string[] {"Ac","Ag","Al","Am","Ar","As","At","Au","B","Ba","Be","Bh","Bi","Bk","Br","C","Ca","Cd","Ce","Cf","Cl","Cm","Cn","Co","Cr","Cs","Cu","Db","Ds","Dy","Er","Es","Eu","F","Fe","Fl","Fm","Fr","Ga","Gd"
            ,"Ge","H","He","Hf","Hg","Ho","Hs","I","In","Ir","K","Kr","La","Li","Lr","Lu","Lv","Mc","Md","Mg","Mn","Mo","Mt","N","Na","Nb","Nd","Ne","Nh","Ni","No","Np","O","Og","Os","P","Pa","Pb","Pd","Pm","Po","Pr","Pt","Pu","Ra","Rb","Re"
            ,"Rf","Rg","Rh","Rn","Ru","S","Sb","Sc","Se","Sg","Si","Sm","Sn","Sr","Ta","Tb","Tc","Te","Th","Ti","Tl","Tm","Ts","U","V","W","Xe","Y","Yb","Zn","Zr"};

        static List<string> MaximoEncontrado = new List<string>();

        public TablaPeriodica()
        {
            InitializeComponent();
        }


        public static List<string> CalcularListaTabla(List<string> Elementos, int IndiceEmpezar)
        {
            Elementos = Elementos.Select(s => s.ToUpper()).ToList();
            List<string> calculando = new List<string>();

            //Recorremos elemento por elemento:
            for (int i = 0; i < Elementos.Count(); i++)
            {              
                string ultimo = calculando[calculando.Count() - 1].ToUpper();
                char acabaEn = ultimo.LastOrDefault();
                string siguiente = Elementos.Where(w => w != ultimo && w.StartsWith(acabaEn.ToString())).DefaultIfEmpty(null).First();

                siguiente = Elementos.Where(w => w != ultimo && !calculando.Contains(w) && w.StartsWith(acabaEn.ToString())).DefaultIfEmpty(null).First();
                
                if(siguiente == null)
                { //No hay elementos ya que empiecen por acabaEn:
                    if(calculando.Count() > MaximoEncontrado.Count())
                    {
                        MaximoEncontrado = calculando;
                    }
                    else
                    {
                        calculando = new List<string>();
                        //tabla = TablaElementos.Select(s => s.ToUpper()).ToList();
                    }
                }
                else
                { //Añadimos el elemento:
                    calculando.Add(siguiente);
                    //tabla.RemoveAt(i);
                }
            }

            return Elementos;
        }



        private void btnCalcular_Click(object sender, EventArgs e)
        {
            List<string> lista = CalcularListaTabla(TablaElementos.ToList(),0);
            txtResultado.Text = string.Concat('-', lista);
        }
    }
}
